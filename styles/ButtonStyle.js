import React, { StyleSheet } from 'react-native';

export const ButtonStyle = StyleSheet.create
({
    buttonRound:
    {
        backgroundColor: '#47525E',
        height: '85%',
        borderRadius: 50
    }
})
