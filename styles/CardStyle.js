import React, { StyleSheet } from 'react-native';

export const CardStyle = StyleSheet.create
({
    dareCard:
    {
        flex: 0,
        marginTop: 0,
        height: '70%',
        backgroundColor: '#BDC7C1',
        borderRadius: 30,
        justifyContent: 'center', 
        alignItems: 'center'
    }
})
