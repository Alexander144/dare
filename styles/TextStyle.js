import React, { StyleSheet } from 'react-native';

export const TextStyle = StyleSheet.create
({
    headerTitle:
    {
        fontSize: 100
    },
    headerTitle2:
    {
        fontSize: 30
    },
    headerTitle3:
    {
        fontSize: 20
        
    },
    inputfieldText:
    {
        flex: 0.2,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1,
        marginTop: 30
    },
    positionLeftText: 
    {
        flex: 1, 
        fontSize: 15
    },
    positionRightText: 
    {
        flex: 1,
        fontSize: 15
    },
})
