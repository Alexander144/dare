import React, { StyleSheet } from 'react-native';

export const ViewStyle = StyleSheet.create
({
    rootDefault:
    {
        flex: 1,
        marginTop: 50, 
        backgroundColor: '#BDC7C1'
    },
    centerContent:
    {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    bottomContent:
    {
        bottom: 200,
        width: '100%',
        position: "absolute",
    },
    horizontalContent: 
    {
        flexDirection: 'row',
        flex: 2
    }
})