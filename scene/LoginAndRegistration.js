import { mapDispatchToProps } from "../app/lib/map";
import LoginContainer from "../app/containers/LoginContainer";
import { ViewStyle } from "../styles/ViewStyle";
import { TextStyle } from "../styles/TextStyle";

import { connect } from "react-redux";
import React, { Component } from 'react';
import 
{
    View,
    Text,
} from "react-native";


export class LoginAndRegistration extends Component
{
    constructor(props)
    {
        super(props);
    }
    
    render()
    {
        return <View style = { ViewStyle.rootDefault } >
            <View style = { ViewStyle.centerContent } >
                <Text style = { TextStyle.headerTitle }> Dare </Text>
            </View>
            <LoginContainer {...this.props} />
        </View>
    }
}

export default connect((state) => {
    return {
        
    }
}, mapDispatchToProps)(LoginAndRegistration);

