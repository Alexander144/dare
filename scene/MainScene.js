import { mapDispatchToProps } from "../app/lib/map";
import { TabNavigation } from "../app/lib/navigation";
import { ViewStyle } from "../styles/ViewStyle";
import { TextStyle } from "../styles/TextStyle";

import { connect } from "react-redux";
import React, { Component } from 'react';
import { ButtonGroup } from 'react-native-elements';
import 
{
    View,
    Text,
} from "react-native";


export class MainScene extends Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        const buttons = ['Give Me A Dare', 'News Feed'];
        return <View style = { ViewStyle.rootDefault } >
            <TabNavigation/>
        </View>
    }
}

export default connect((state) => {
    return {
        
    }
}, mapDispatchToProps)(MainScene);

