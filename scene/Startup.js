/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import reducer from "../app/reducers";
import { StackNavigation } from "../app/lib/navigation";

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReduxers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__  });

const store = configureStore({});

function configureStore(initialState)
{
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
    ),
  );
  return createStore(reducer, initialState, enhancer);
}

export default Startup = () =>
(
  <Provider store = {store}>
    <StackNavigation />
  </Provider>
); 
