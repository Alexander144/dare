import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import firebase from '../lib/firebase';

export const searchedUsers = createReducer({}, {
    
});

export const user = createReducer({authorizing: false}, {
    [types.USER_START_AUTHORIZING](state, action){
        return Object.assign({}, state, {
            authorizing: true
        });
    },
    [types.USER_AUTHORIZED](state, action){
        return Object.assign({}, state, {
        authorizing: false,
        authorized: true
        });
    },
    [types.SET_USER_NAME](state, action){
            return Object.assign({}, state, {
                name: action.name
            });
    },
    [types.USER_NO_EXIST](state, action){
            return Object.assign({}, state, {
                authorizing: false,
                authorized: false
            });
    }
});

export const UserCount = createReducer(0, {
    [types.ADD_USER](state, action){
        return state + 1;
    }
});