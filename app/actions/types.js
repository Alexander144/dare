
export const ADD_USER = 'ADD_USER';

export const USER_NO_EXIST = 'USER_NO_EXIST';

export const SET_USER_NAME = 'SET_USER_NAME';

export const LOGIN = 'LOGIN';

export const USER_START_AUTHORIZING = 'USER_START_AUTHORIZING';

export const USER_AUTHORIZED = 'USER_AUTHORIZED';

