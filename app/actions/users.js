import * as types from "./types";
import firebase from "../lib/firebase";

export function AddUser()
{
    return {
        type: types.ADD_USER,
    }
}

export function login(email, password) {
    return(dispatch, getState) => {
        dispatch(startAuthorizing());

        firebase.auth()
                .signInWithEmailAndPassword(email, password)
                .then(() => dispatch(userAuthorized())).catch(() => dispatch(userNotAuthorized()));
    }
}

export function startAuthorizing(){
   return {
       type: 'USER_START_AUTHORIZING'
   }
};

export function userAuthorized(){
   return { 
       type: 'USER_AUTHORIZED'
   }
};

export function userNotAuthorized(){
   return {
       type: 'USER_NO_EXIST'
   }
};