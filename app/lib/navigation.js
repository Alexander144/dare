import { StackNavigator, TabNavigator } from 'react-navigation';

import LoginAndRegistration from "../../scene/LoginAndRegistration";
import MainScene from "../../scene/MainScene";
import DareContainer from "../containers/DareContainer";
import NewsFeedContainer from "../containers/NewsFeedContainer";

export const StackNavigation = StackNavigator({
    
    LoginAndRegistration: 
    { 
      screen: LoginAndRegistration,
      navigationOptions:
      {
         header: null
      }
    },

    MainScene: 
    { 
      screen: MainScene,
      navigationOptions:
       {
          header: null
       },
    },
  });

export const TabNavigation = TabNavigator({
    
    DareContainer: 
    { 
      screen: DareContainer,
      navigationOptions:
       {
          title: 'Give me a dare!',
          header: null
       },
    },

    NewsFeedContainer: { 
      screen: NewsFeedContainer, 
      navigationOptions: 
      { 
        title: 'Newsfeed',
        header: null
      }
    },
  });