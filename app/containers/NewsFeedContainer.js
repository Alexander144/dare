import { mapDispatchToProps } from "../lib/map";
import { ViewStyle } from "../../styles/ViewStyle";
import { DareCard } from "../components/DareCard";

import { connect } from "react-redux";
import React, { Component } from 'react';
import 
{
    View,
    Text,
    Button,
} from "react-native";


class NewsFeedContainer extends Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return <View style = { ViewStyle.rootDefault }>
                <DareCard />
            </View>
    }
}

export default connect((state) => {
    return {
        
    }
}, mapDispatchToProps)(NewsFeedContainer);