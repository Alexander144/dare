import { mapDispatchToProps } from "../lib/map";
import RoundButton from "../components/RoundButton";
import { CardStyle } from "../../styles/CardStyle";
import { TextStyle } from "../../styles/TextStyle";
import { ViewStyle } from "../../styles/ViewStyle";

import { connect } from "react-redux";
import { Card } from 'react-native-elements';
import React, { Component } from 'react';
import 
{
    View,
    Text,
    TextInput
} from "react-native";

class DareContainer extends Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return <View style = {{ flex: 1}}>
                <Card containerStyle = {CardStyle.dareCard}>
                        <Text style = {TextStyle.headerTitle2} > I Dare you To Skydive </Text>
                </Card>
                <View style = {ViewStyle.bottomContent}>
                    <RoundButton />
                    <RoundButton />
                </View>
            </View>
    }
}

export default connect((state) => {
    return {
    }
}, mapDispatchToProps)(DareContainer);