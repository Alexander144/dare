import { mapDispatchToProps } from "../lib/map";
import { ButtonStyle } from "../../styles/ButtonStyle";
import { TextStyle } from "../../styles/TextStyle";

import { connect } from "react-redux";
import { Button } from 'react-native-elements';
import React, { Component } from 'react';
import 
{
    View,
    Text,
    TextInput
} from "react-native";

class LoginContainer extends Component
{
    constructor(props)
    {
        super(props);
    }

    componentWillReceiveProps(props)
    {
        if(props.userAuthorized)
        {
            this.props.navigation.navigate('MainScene');
        }
    }

    loginUser()
    {
        this.props.login(this.state.email, this.state.password);
    }

    render()
    {
        return <View style = {{ flex: 2}}>
                    <Text> user: { String(this.props.userAuthorized) } </Text>
                    <TextInput style = { TextStyle.inputfieldText } placeholder = "Email" onChangeText = {(text) => this.setState({email: text})} />
                    <TextInput style = { TextStyle.inputfieldText } placeholder = "Password" secureTextEntry = { true } onChangeText = {(text) => this.setState({password: text})} />
                    <View style = {{ flex: 1, marginTop: 10 }}>
                        <View style = {{ flex: 0.4 }}>
                            <Button buttonStyle = { ButtonStyle.buttonRound } title = "Logg in" onPress = {() => {this.loginUser(); } } > </Button>
                        </View>
                    </View>
            </View>
    }
}


function mapStateToProps(state) {
    return {
      userAuthorized: state.user.authorized
    };
  }

export default connect(mapStateToProps)(LoginContainer);