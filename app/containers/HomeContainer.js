import { mapDispatchToProps } from "../lib/map";
import DareContainer from "./DareContainer";
import { ViewStyle } from "../../styles/ViewStyle";
import { TextStyle } from "../../styles/TextStyle";

import { connect } from "react-redux";
import React, { Component } from 'react';
import 
{
    View,
    Text,
} from "react-native";


class HomeContainer extends Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return <View style = { ViewStyle.rootDefault }>
                <DareContainer />
        </View>
    }
}

export default connect((state) => {
    return {
        
    }
}, mapDispatchToProps)(HomeContainer);