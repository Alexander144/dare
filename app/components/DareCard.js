import { mapDispatchToProps } from "../lib/map";
import { ButtonStyle } from "../../styles/ButtonStyle";
import { ViewStyle } from "../../styles/ViewStyle";
import { CardStyle } from "../../styles/CardStyle";
import { TextStyle } from "../../styles/TextStyle";
import { ImageStyle } from "../../styles/ImageStyle";

import { connect } from "react-redux";
import React, { Component } from 'react';
import { Card } from 'react-native-elements';
import 
{
    View,
    Text,
    Image
} from "react-native";

export class DareCard extends Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return <View>
                    <Card containerStyle = { CardStyle.dareCard }>
                        <View style = { ViewStyle.horizontalContent }>
                            <Text style = { TextStyle.positionLeftText }> Alexander </Text>
                            <Text style = { TextStyle.positionRightText }> Time </Text>
                        </View>
                       
                        <View style = {{flex: 7}}>
                            <Text style = {TextStyle.headerTitle3} > I Dare you To Skydive </Text>
                        </View>

                        <View style = {{flex: 20}}>
                            <Image style = { ImageStyle.cardImage } source={{ uri: "https://upload.wikimedia.org/wikipedia/en/e/eb/Spaceball_jump_over_Skydive_35.jpg" }} />
                        </View>
                    </Card>
            </View>
    }
}

export default connect((state) => {
    return {
    }
}, mapDispatchToProps)(DareCard);