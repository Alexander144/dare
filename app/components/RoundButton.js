import { mapDispatchToProps } from "../lib/map";
import { ButtonStyle } from "../../styles/ButtonStyle";

import { connect } from "react-redux";
import React, { Component } from 'react';
import { Button } from 'react-native-elements';
import 
{
    View,
} from "react-native";

export class RoundButton extends Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return <View>
                <Button buttonStyle = { ButtonStyle.buttonRound } title = "Logg in" onPress = {() => console.log("Hello")} > </Button>
             </View>
    }
}

export default connect((state) => {
    return {
    }
}, mapDispatchToProps)(RoundButton);